        .global correct16

correct16:
        push    {LR}
        push    {R1-R12}            @ saves the content of LR and R1-R12

        mov     R1, #0
        mov     R2, #0              @ resets all registers to 0
        mov     R3, R0              @ initializes registers
        mov     R0, #0
        mov     R4, #1
        mov     R5, #0
        mov     R6, #20
        mov     R7, #0
        mov     R8, #0
        mov     R9, #0
        mov     R10, #0
        mov     R11, #0
        mov     R12, #0

@R5 will store the position of corrupted bit
p16:    @ calculates parity bit at position 15 and verifies it
        and     R2, R4, R3, lsr R6

        cmp     R2, #1
        addeq   R1, #1

        sub     R6, #1
        cmp     R6, #16
        bge     p16

        bl      modulus

p8:     @ calculates parity bit at position 7 and verifies it 
        and     R2, R4, R3, lsr R6

        cmp     R2, #1
        addeq   R1, #1

        sub     R6, #1
        cmp     R6, #8
        bge     p8

        bl      modulus

        mov     R6, #20

p4:     @calculates parity bit at position 3 and verifies it
        and     R2, R4, R3, lsr R6

        cmp     R2, #1
        addeq   R1, #1

        cmp     R6, #19
        subeq   R6, #5
        beq     p4

        cmp     R6, #11
        subeq   R6, #5
        beq     p4

        sub     R6, #1
        cmp     R6, #4
        bge     p4

        bl      modulus
        mov     R6, #18

p2:     @calculates parity bit at position 1 and verifies it
        and     R2, R4, R3, lsr R6

        cmp     R2, #1
        addeq   R1, #1

        cmp     R6, #17
        subeq   R6, #3
        beq     p2

        cmp     R6, #13
        subeq   R6, #3
        beq     p2

        cmp     R6, #9
        subeq   R6, #3
        beq     p2

        cmp     R6, #5
        subeq   R6, #3
        beq     p2

        sub     R6, #1
        cmp     R6, #2
        bge     p2

        bl      modulus
        mov     R6, #20

p1:     @calculates parity bit at position 0 and verifies it
        and     R2, R4, R3, lsr R6

        cmp     R2, #1
        addeq   R1, #1    

        sub     R6, #2
        cmp     R6, #2
        bge     p1

        bl      modulus
        lsr     R5, #1 
        sub     R5, #1
        mov     R6, #20

display:@ DO NOT ADD CHECK BITSSS
        and     R2, R4, R3, lsr R6

        cmp     R6, #1
        beq     exit

        cmp     R6, #15
        subeq   R6, #1
        beq     display

        cmp     R6, #7
        subeq   R6, #1
        beq     display

        cmp     R6, #3
        subeq   R6, #1
        beq     display

        cmp     R5, R6
        eoreq   R2, #1
        add     R0, R2
        lsl     R0, #1
        subs    R6, #1
        bge     display
    

exit:   @ exits subroutine and prints out the value
        lsr     R0, #1
        pop     {R1-R12}    @ retrieves contents of registers back
        pop     {LR}

        bx      LR

modulus: @ counts number of 1s in a register then verifies parity
        
        subs    R1, #2
        bgt     modulus

        cmp     R1, #0
        moveq   R12, #0
        movne   R12, #1
        mov     R1, #0
        and     R2, R4, R3, lsr R6
        sub     R6, #1
        
        cmp     R2, R12
        addne   R5, #1
        lsl     R5, #1

        bx      LR


        
        